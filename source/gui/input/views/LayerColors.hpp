#pragma once
#include "bow/input/Layers.hpp"
#include <QtWidgets>

QColor getLayerColor(const Layer& layer);
QPixmap getLayerPixmap(const Layer& layer);
